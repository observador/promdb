// Package promdb exposes custom Prometheus collectors that export database metrics.
package promdb

import (
	"database/sql"

	"github.com/prometheus/client_golang/prometheus"
)

// Kind is an enum representing the different types of supported databases
type Kind uint

// Supported databases
const (
	_ Kind = iota
	MySQL
	Postgres
)

func (k Kind) String() string {
	switch k {
	case MySQL:
		return "mysql"
	case Postgres:
		return "postgres"
	default:
		return "unknown"
	}
}

type sqlCollector struct {
	db                *sql.DB
	maxOpen           *prometheus.Desc
	maxIdleClosed     *prometheus.Desc
	maxIdleTimeClosed *prometheus.Desc
	maxLifetimeClosed *prometheus.Desc
	open              *prometheus.Desc
	inUse             *prometheus.Desc
	idle              *prometheus.Desc
	waitCount         *prometheus.Desc
	waitDuration      *prometheus.Desc
}

// NewSQLCollector returns a collector that exports metrics about the given
// sql.DB instance. This includes all the stats advertised by db.Stats().
func NewSQLCollector(kind Kind, name string, db *sql.DB) prometheus.Collector {
	labels := prometheus.Labels{
		"db":   name,
		"kind": kind.String(),
	}

	return &sqlCollector{
		db:                db,
		maxOpen:           prometheus.NewDesc("sql_max_open_connections", "Maximum number of open connections to the database.", nil, labels),
		maxIdleClosed:     prometheus.NewDesc("sql_max_idle_closed", "The total number of connections closed due to SetMaxIdleConns.", nil, labels),
		maxIdleTimeClosed: prometheus.NewDesc("sql_max_idle_time_closed", "The total number of connections closed due to SetConnMaxIdleTime.", nil, labels),
		maxLifetimeClosed: prometheus.NewDesc("sql_max_lifetime_closed", "The total number of connections closed due to SetConnMaxLifetime.", nil, labels),
		open:              prometheus.NewDesc("sql_open_connections", "The number of established connections both in use and idle.", nil, labels),
		inUse:             prometheus.NewDesc("sql_in_use", "The number of connections currently in use.", nil, labels),
		idle:              prometheus.NewDesc("sql_idle", "The number of idle connections.", nil, labels),
		waitCount:         prometheus.NewDesc("sql_wait_count", "The total number of connections waited for.", nil, labels),
		waitDuration:      prometheus.NewDesc("sql_wait_duration", "The total time blocked waiting for a new connection in seconds.", nil, labels),
	}
}

func (c *sqlCollector) Describe(ch chan<- *prometheus.Desc) {
	ch <- c.maxOpen
	ch <- c.maxIdleClosed
	ch <- c.maxIdleTimeClosed
	ch <- c.maxLifetimeClosed
	ch <- c.open
	ch <- c.inUse
	ch <- c.idle
	ch <- c.waitCount
	ch <- c.waitDuration
}

func (c *sqlCollector) Collect(ch chan<- prometheus.Metric) {
	stats := c.db.Stats()
	ch <- prometheus.MustNewConstMetric(c.maxOpen, prometheus.CounterValue, float64(stats.MaxOpenConnections))
	ch <- prometheus.MustNewConstMetric(c.maxIdleClosed, prometheus.CounterValue, float64(stats.MaxIdleClosed))
	ch <- prometheus.MustNewConstMetric(c.maxIdleTimeClosed, prometheus.CounterValue, float64(stats.MaxIdleTimeClosed))
	ch <- prometheus.MustNewConstMetric(c.maxLifetimeClosed, prometheus.CounterValue, float64(stats.MaxLifetimeClosed))
	ch <- prometheus.MustNewConstMetric(c.open, prometheus.GaugeValue, float64(stats.OpenConnections))
	ch <- prometheus.MustNewConstMetric(c.inUse, prometheus.GaugeValue, float64(stats.InUse))
	ch <- prometheus.MustNewConstMetric(c.idle, prometheus.GaugeValue, float64(stats.Idle))
	ch <- prometheus.MustNewConstMetric(c.waitCount, prometheus.CounterValue, float64(stats.WaitCount))
	ch <- prometheus.MustNewConstMetric(c.waitDuration, prometheus.CounterValue, stats.WaitDuration.Seconds())
}
