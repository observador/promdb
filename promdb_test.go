package promdb_test

import (
	"database/sql"

	"bitbucket.org/observador/promdb"
	"github.com/prometheus/client_golang/prometheus"
)

func Example() {
	db, _ := sql.Open("mysql", "bob:password@tcp(127.0.0.1)/bobs_data")

	prometheus.MustRegister(promdb.NewSQLCollector(promdb.MySQL, "bobs_data", db))

	// Will expose the following metrics:
	//
	// # HELP sql_idle The number of idle connections.
	// # TYPE sql_idle gauge
	// sql_idle{db="bobs_data",kind="mysql"} 1
	//
	// # HELP sql_in_use The number of connections currently in use.
	// # TYPE sql_in_use gauge
	// sql_in_use{db="bobs_data",kind="mysql"} 0
	//
	// # HELP sql_max_idle_closed The total number of connections closed due to SetMaxIdleConns.
	// # TYPE sql_max_idle_closed counter
	// sql_max_idle_closed{db="bobs_data",kind="mysql"} 0
	//
	// # HELP sql_max_lifetime_closed The total number of connections closed due to SetConnMaxLifetime.
	// # TYPE sql_max_lifetime_closed counter
	// sql_max_lifetime_closed{db="bobs_data",kind="mysql"} 0
	//
	// # HELP sql_max_open_connections Maximum number of open connections to the database.
	// # TYPE sql_max_open_connections counter
	// sql_max_open_connections{db="bobs_data",kind="mysql"} 26
	//
	// # HELP sql_open_connections The number of established connections both in use and idle.
	// # TYPE sql_open_connections gauge
	// sql_open_connections{db="bobs_data",kind="mysql"} 1
	//
	// # HELP sql_wait_count The total number of connections waited for.
	// # TYPE sql_wait_count counter
	// sql_wait_count{db="bobs_data",kind="mysql"} 0
	//
	// # HELP sql_wait_duration The total time blocked waiting for a new connection in seconds.
	// # TYPE sql_wait_duration counter
	// sql_wait_duration{db="bobs_data",kind="mysql"} 0
}
